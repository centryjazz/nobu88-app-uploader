package com.multipolar.nobu88appuploader;

import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;

public class splash_screen extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);
        Thread thread = new Thread(){
            public void run(){
                try {
                    sleep(2500);

                }catch (InterruptedException e){
                    e.printStackTrace();
                }finally {
                    startActivity(new Intent(splash_screen.this,MainActivity.class));
                    finish();
                }
            }
        };
        thread.start();
    }

}

